# dotfiles

## Install
It is important that this is in $HOME

```
sudo apt install git
git clone https://github.com/cguo5520/dotfiles.git dotfiles
cd dotfiles
bash setup.sh
```

## Set up SSH keys
The Public Key should be in your clipboard already.
Go to https://gitlab.com/-/profile/keys and https://github.com/settings/keys to upload your public ssh key.

## Post Install
```
bash post_install.sh
```

* Download the KeepassXC and key from gDrive

## Set up gCloud defaults and credentials
```
gcloud init
```

## Google Chrome setup

1. Go to google chrome and sign in to user account to get all extensions
2. Click on Vimium-C extension => options => import settings (at bottom) => restore from dotfiles/vimium_backup.json
3. Enable chrome://flags/#extensions-on-chrome-urls

## Gsettings / dconf
run `dconf_dump.sh` to back up your current dconf

### Overrides
Run your overrides in `gsettings_override.sh` to customize per-computer

## Notes

* You can also run single playbooks like:
```
bash setup.sh copy
```

## TODO:
Faster repeating keypresses
per-machine mouse speed
fix vim vscode config for y key
fix .vimrc for delete
can't type question mark in vscode
can't accept a suggestion with enter
can't tab through suggestions with tab
