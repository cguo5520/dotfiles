# daru

VsCode settings for the discerning weaboo hacker.

Vim-like text editing language created for Colemak, VsCode. Emacs-like layer for everything else.

## How to Read This

|char = former vim command. Use for easy searching.

C- = ctrl
  
## Common Meanings
### Universal Escape (VsCode, Vim, etc.)
    C-space
### Special
    space = leader
### Verbs
    s = spawn / open component
    d = duplicate / spawn downward
    k = kill / close component

    double of any letter combo is to do the most common operation
### Nouns
   Generally, ctrl in front of anything means that it's a VsCode component or extension

## Usage
bash daru_install.sh

## Future directions
    We can get around the keymapping bugs in vscode by mapping certain keys to a macro. And then the macro plugin can type leader, letter to simulate an operator. I'm not sure this is the right direction because Terminal cannot work this way AFAIK. We should definitely make folding go back to a command in Vim and then move back search to C-f. Also, we need to get rid of the random commands that VsCode comes with re: list and others.

    For keybindings, alt a is the mapping for TODO

## Plugins
See ansible

## External dependencies
apt install xsel

## To unmap all default keybindings
Get default keybindings from https://github.com/codebling/vs-code-default-keybindings
Put unmappings in unmap.all.keybindings.json
Then use dedupeUnmaps.js to get deduped

## TODO
question mark doesn't work in insert mode
Wipe all keycodes better
Wipe all settings better
