
module.exports = function (keybindingsPath) {
  const { readAbsoluteFile, writeFile } = require('./fsUtil');

  const unmappings = [];
  const mappings = [];

  // keybindings file must not have comments
  const keybindingsRawStr = readAbsoluteFile(keybindingsPath);
  keybindings = JSON.parse(keybindingsRawStr);

  keybindings.forEach(binding => {
    if (binding.command[0] === '-') {
      unmappings.push(binding);
    } else {
      mappings.push(binding);
    }
  });

  function formatBindings(bindingArr) {
    return bindingArr.map(binding => `${binding.key}\t${binding.command}\t${binding.when}`)
      .sort((a, b) => {
        if (a > b) return 1;
        return -1;
      })
      .join('\n');
  }

  const formattedUnmappings = formatBindings(unmappings);
  const formattedMappings = formatBindings(mappings)

  writeFile('outputs/unmapped_keybindings_diagnostics.txt', formattedUnmappings);
  writeFile('outputs/mapped_keybindings_diagnostics.txt', formattedMappings);

  console.log('finished outputting diagnostics');
}
