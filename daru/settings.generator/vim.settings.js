module.exports = function generateVimSettings() {
  const vimSettings = {
    // vim.debug.silent
    // vim.debug.loggingLevelForAlert
    // vim.debug.loggingLevelForConsole
    // vim.normalModeKeyBindings
    // vim.normalModeKeyBindingsNonRecursive
    // vim.operatorPendingModeKeyBindings
    // vim.operatorPendingModeKeyBindingsNonRecursive	
    "vim.useCtrlKeys": true,
    // vim.leader
    // vim.searchHighlightColor
    // vim.searchHighlightTextColor
    "vim.highlightedyank.enable": true,
    "vim.highlightedyank.color": "#ff0000",
    "vim.highlightedyank.textColor": "#000000",
    "vim.highlightedyank.duration": 5000, // Duration in ms
    "vim.useSystemClipboard": true, // Use system clipboard for unnamed register
    // vim.overrideCopy	
    // vim.insertModeKeyBindings	
    // vim.insertModeKeyBindingsNonRecursive	
    // vim.visualModeKeyBindings	
    // vim.visualModeKeyBindingsNonRecursive	
    // vim.commandLineModeKeyBindings	
    // vim.commandLineModeKeyBindingsNonRecursive	
    // vim.textwidth	
    // vim.timeout
    // vim.maxmapdepth
    // vim.scroll	
    // vim.showcmd 
    // vim.showmodename
    // vim.iskeyword	
    // vim.ignorecase
    // vim.smartcase	
    // vim.camelCaseMotion.enable	
    "vim.easymotion": true,
    "vim.easymotionMarkerBackgroundColor": "#ffb400",
    "vim.easymotionMarkerForegroundColorOneChar": "#000000",
    "vim.easymotionMarkerForegroundColorTwoChar": "#000000",
    "vim.easymotionMarkerForegroundColorTwoCharFirst": "#000000",
    "vim.easymotionMarkerForegroundColorTwoCharSecond": "#000000",
    // vim.easymotionIncSearchForegroundColor
    "vim.easymotionDimColor": "#777777",
    // vim.easymotionMarkerWidthPerChar
    "vim.easymotionDimBackground": true, // dim other text while markers are visible
    // vim.easymotionMarkerFontFamily
    // vim.easymotionMarkerFontSize
    // vim.easymotionMarkerFontWeight
    // vim.easymotionMarkerMargin
    "vim.easymotionKeys": "arstneio",
    "vim.easymotionJumpToAnywhereRegex": "def|func|\/\/|#", //	Regex matches for JumpToAnywhere motion. Currently set to jump to function defs
    // vim.replaceWithRegister
    // vim.smartRelativeLine
    // vim.sneak
    // vim.sneakUseIgnorecaseAndSmartcase
    // vim.sneakReplacesF
    // vim.surround
    // vim.argumentObjectSeparators
    // vim.argumentObjectOpeningDelimiters
    // vim.argumentObjectClosingDelimiters
    // vim.hlsearch
    // vim.incsearch
    "vim.history": 0,
    "vim.autoindent": true,
    "vim.joinspaces": false, // If true, add two spaces after '.', '?', and '!' when joining or reformatting
    "vim.startInInsertMode": false,
    // vim.handleKeys
    "vim.statusBarColorControl": false, // This is nice, but has a negative effect on performance
    // "vim.statusBarColors.normal": "#000000",
    // "vim.statusBarColors.insert": "#ff0000",
    // "vim.statusBarColors.visual": "#0000ff",
    // "vim.statusBarColors.visualline": "#ff00ff",
    // "vim.statusBarColors.visualblock": "#fff000",
    // "vim.statusBarColors.replace": "#00ff00",
    // vim.statusBarColors.commandlineinprogress
    // vim.statusBarColors.searchinprogressmode
    // vim.statusBarColors.easymotionmode
    // vim.statusBarColors.easymotioninputmode
    // vim.statusBarColors.surroundinputmode
    // vim.visualstar
    // vim.changeWordIncludesWhitespace
    // vim.foldfix This is hacky. Just don't use foldfix
    "vim.mouseSelectionGoesIntoVisualMode": true,
    "vim.disableExtension": false,
    // vim.enableNeovim
    // vim.neovimPath
    // vim.neovimUseConfigFile
    // vim.neovimConfigPath
    // vim.vimrc.enable	
    // vim.vimrc.path	
    // vim.substituteGlobalFlag	
    // vim.gdefault	
    "vim.cursorStylePerMode.normal": "block",
    "vim.cursorStylePerMode.insert": "block-outline",
    "vim.cursorStylePerMode.replace": "block-outline",
    "vim.cursorStylePerMode.visual": "block-outline",
    "vim.cursorStylePerMode.visualline": "block-outline",
    "vim.cursorStylePerMode.visualblock": "block-outline",
    // vim.autoSwitchInputMethod.enable
    // vim.autoSwitchInputMethod.defaultIM
    // vim.autoSwitchInputMethod.switchIMCmd
    // vim.autoSwitchInputMethod.obtainIMCmd
    // vim.whichwrap
    // vim.report
    // vim.digraphs
    // vim.wrapscan
    // vim.startofline
    // vim.showMarksInGutter
  };
  return vimSettings;
};