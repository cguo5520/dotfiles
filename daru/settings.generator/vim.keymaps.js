const bindings = {
  // C-space => escape handled by vscode keybindings
  'normal visual': {
    maps: {
      a: '', // put this here so if you mess up a digraph by only pressing 'a' once, it does nothing
      u: 'i',
      y: 'a',
      N: 'b',
      E: '1 0 j',
      I: '1 0 k',
      O: 'e',
      'l l': 'J',
      'l n': 'i <CR> <Esc>', // break line before cursor
      'l o': 'a <CR> <Esc>', // break line after cursor
      'l e': '$ a <CR> <Esc>',
      'l i': '0 i <CR> <Esc>',
      v: 'p',
      "\'": ';',
      '\"': ',',
      't p': '%',
      't e': 'G',
      't i': 'g g',
      't n': '^',
      't o': '$',
      's': '\\ \\ 2 s', // two character easymotion search
      'S': '\\ \\ \\ j', // see vim.easymotionJumpToAnywhereRegex
      '<space>': 'i <space> <Esc> l', // type space
    },
    commands: {
      'a a': 'workbench.action.files.save',
      'a f': 'editor.action.formatDocument',
      'k': 'workbench.action.closeActiveEditor',
      'a n': 'workbench.action.previousEditor',
      'a o': 'workbench.action.nextEditor',
      'a p': 'workbench.action.files.copyPathOfActiveFile',
      'a q': 'workbench.action.closeOtherEditors',
      // 'a t': 'extension.toggleTheme',
      'f c': 'toggleFindCaseSensitive',
      'f f': 'actions.find',
      'f r': 'editor.action.startFindReplaceAction',
      'f w': 'toggleFindWholeWord',
      'f x': 'toggleFindRegex',
      h: 'editor.action.commentLine',
      't b': 'editor.action.jumpToBracket',
      't d': 'editor.action.goToDeclaration',
      't f': 'workbench.action.quickOpen',
      't l': 'workbench.action.gotoLine',
      't t': 'workbench.action.quickOpen',
      't y': 'editor.action.goToTypeDefinition',
      // fold the end bracket of the block you just folded to fold the next layer up
      'w n': 'editor.foldAll',
      'w e': 'editor.unfold',
      'w i': 'editor.foldRecursively',
      'w o': 'editor.unfoldAll',
      // '<backspace>': 'deleteLeft', // This doesn't work
      'z': 'undo',
      'Z': 'redo',
    }
  },
  normal: {
    maps: {
      m: 'v',
      ',': 'V',
      'b': 'C-v',
      '.': 'R',
      'c': 'y',
      'c c': 'y y',
      'x': 'd',
      'x x': 'd d',
      'd': '" _ d', // d keys mirror x keyset but to the 0 register
      'd d': '" _ d d',
    },
    commands: {
      n: 'cursorLeft', // vim directions auto-expand folded code blocks
      e: 'cursorDown',
      i: 'cursorUp',
      o: 'cursorRight',
    }
  },
  visual: {
    maps: {
      n: 'h', // normal vim mappings don't destroy line and block mode functionality
      e: 'j',
      i: 'k',
      o: 'l',
      x: 'd',
      c: 'y',
      d: '" _ d',
    },
    commands: {}
  },
  operatorPending: {
    maps: {
      w: 'a w',
      n: 'b',
      e: '$',
      i: '^',
      o: 'e',
      // ADD HANDLING FOR BRACKETS. Should be able to jump to a bracket without losing the highlighting
      // % doesn't work the way I want. Should also work with quotations and stuff
    },
    commands: {}
  },
};

module.exports = function () {
  const settings = {};

  // init
  ['insert', 'normal', 'visual', 'operatorPending'].forEach(modeName => {
    settings[`vim.${modeName}ModeKeyBindingsNonRecursive`] = [];
  });

  // expanded
  Object.entries(bindings).forEach(([modeNames, mode]) => {
    const expandedBindings = [];

    if (mode.maps) {
      Object.entries(mode.maps).forEach(([before, after]) => {
        const binding = { before: before.split(' '), after: after.split(' ') };
        expandedBindings.push(binding);
      });
    }

    if (mode.commands) {
      Object.entries(mode.commands).forEach(([before, commands]) => {
        const binding = { before: before.split(' '), commands: commands.split(' ') };
        expandedBindings.push(binding);
      });
    }

    modeNames.split(' ').forEach(modeName => {
      settings[`vim.${modeName}ModeKeyBindingsNonRecursive`].push(...expandedBindings);
    });
  });

  return settings;
};
