module.exports = function (settingsPath) {
  const { writeFile, writeAbsoluteJSON } = require('../fsUtil');

  const misc = require('./misc.settings');

  const leaderPrefixedKeys = [];

  const genVimSettings = require('./vim.settings');
  const vimSettings = genVimSettings();

  const genVimKeys = require('./vim.keymaps');
  const vimKeys = genVimKeys(leaderPrefixedKeys);

  const finalObj = Object.assign(misc, vimSettings, vimKeys);
  const OUTPUT_PATH = 'outputs/settings.output.json';

  writeFile(OUTPUT_PATH, finalObj);

  writeAbsoluteJSON(settingsPath, finalObj);

  console.log('successfully overwrite VsCode settings');
}
