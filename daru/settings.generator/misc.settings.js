const defaultCommandsToSkipShell = require('./default.commands.to.skip.shell.json');

module.exports = {
    "breadcrumbs.enabled": true,
    "editor.fontSize": 16,
    "editor.minimap.enabled": false,
    "editor.mouseWheelZoom": false,
    "editor.occurrencesHighlight": false,
    "editor.renderLineHighlight": "all",
    "editor.selectionHighlight": true,
    "editor.tabSize": 2,
    "editor.wordWrap": "on",
    "eslint.validate": ["javascript", "javascriptreact", "vue", "html"],
    "files.useExperimentalFileWatcher": true,
    "keyboard.dispatch": "keyCode",
    "scrolloff.scrolloff": 8,
    "terminal.integrated.commandsToSkipShell": defaultCommandsToSkipShell,
    "terminal.integrated.scrollback": 10000,
    "window.titleBarStyle": "custom",
    "window.zoomLevel": -1,
    "workbench.activityBar.visible": true,
    "workbench.colorTheme": "Night Owl Light",
    "workbench.iconTheme": null,
    "workbench.settings.openDefaultKeybindings": false,
    "workbench.sideBar.location": "left",
    "workbench.list.automaticKeyboardNavigation": false, // type to searc
};
