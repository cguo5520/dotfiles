#!/bin/bash

node mac.js

declare -a arr=(
"sdras.night-owl"
"vscodevim.vim"
"tickleforce.scrolloff"
"golang.go"
"ms-python.python"
"redhat.vscode-yaml"
"dbaeumer.vscode-eslint"
"richie5um2.vscode-sort-json"
"zengxingxin.sort-js-object-keys"
"shakram02.bash-beautify"
"andyyaldoo.vscode-json"
"yzhang.markdown-all-in-one"
"tomphilbin.lodash-snippets"
"christian-kohler.path-intellisense"
"GitHub.copilot"
"naumovs.color-highlight"
)

## now loop through the above array
for i in "${arr[@]}"
do
  code --install-extension "$i"
done
