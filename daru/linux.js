const KEYBINDINGS_PATH = '.config/Code/User/keybindings.json';
require('./keybindings.generator')(KEYBINDINGS_PATH);
require('./settings.generator')('.config/Code/User/settings.json');
require('./keybindings.diagnostics')(KEYBINDINGS_PATH);
