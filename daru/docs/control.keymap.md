# Control Keymap
Control Keymaps work by pressing down control for the first key. They generally denote that VsCodeVim is not handling it.
the second key in the chord/digraph should not need control to be pressed unless it is two of the same key.
### Universal Escape (mapped from anything prior in VsCode that used escape)
C-space
### Unchanged
    z = undo
    x = cut
    c = copy
    v = paste
    Z = redo
### Directions
    n = left / backward
    e = down / last / bottom
    i = up / first / top
    o = right / forward once
### Window Management (What VsCode calls editor groups)
    w w  = hide sidebar and focus editor
    w s  = spawn / split editor group
    w k  = kill editor group
    w neio = move the focus of current editor group
    w z  = toggle zen mode
    w f  = toggle fullscreen
### Search Panel
    f f  = find in all files 
    f s  = spawn search panel / find in all files
    f a  = find in all files
    f k  = hide search panel (when in focus)
    f r  = search and replace in all files
    f g  = Google
    f d  = toggle "detail menu" exclude/include file search
### Explorer Panel (uses list commands in VS)
    l s  = spawn explorer panel (focus on project directory)
    l k  = hide explorer panel (when in focus)
    l f  = new file
    l d  = new directory / folder
    l r  = rename file / folder
    l b  = focus breadcrumbs
    l p  = copy absolute path

    l e  = focus open editors menu
    l o  = outline focus
### Active File (Commands available when right clicking the file tabs at the top of the editor + Misc.)
    a a  = save |zz|:wq
    a s  = open file (this command also in daru)
    a d  = duplicate / save as (useful for duplicating files too)
    a n  = move to editor left
    a o  = move to editor right
    a k  = close editor
    a q  = quit all other editors

    // Misc
    a p  = copy absolute path of active file
    a l  = scrolloff / scroll lock // don't know if I can implement
    a f  = format document
    a h  = format highlighted
### Terminal (integrated VsCode Terminal)
    A A = toggle terminal
    A s = spawn / split terminal
    A k = kill
    A d = new row/set of terminals
    A m = toggle maximize panel

    A n = move focus left terminal
    A o = move focus right terminal
    A e = down one row/set
    A i = up one row/set
### Gitlens
### Docker
### Debugger
