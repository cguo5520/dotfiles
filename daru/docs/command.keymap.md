## Insert Mode
    ii   = escape to normal mode
    C-space = escape to normal mode
    Esc = escape to normal mode
## Command Mode, Normal + Visual
### Mode Switching
    u    = insert mode |i
    U    = insert at beginning of line |I
    y    = append |a
    Y    = append at end of line |A
    m    = visual mode |v
### Moving Around
    neio = left/down/up/right |h|j|k|l
     EI  = bottom of screen / top of screen |L|H
    N  O = beginning of word / end of word |b|e
### Editing
    z    = undo |u
    Z    = redo |C-r
    x    = cut//delete |d
    c    = copy//yank |y
    v    = paste after cursor |p
    ^    = join current line to previous |J
### Misc
    '    = repeat forward find/sneak/till |;
    "    = repeat backward find/sneak/till |,
    Q    = toggle record macro Q<letter for register>[commands]Q |q
    q    = use macro |@
### Find / Sneak Text
    f f  = find in file
    f r  = replace in file
    f n  = find previous
    f e  = find last
    f i  = find first
    f o  = find next
    s{twochars} = sneak // see official VsCodeVim readme
#### to ...
    t t  = to file 
    t f  = to file (as well)
    t l  = to line
    t y  = to type def
    t d  = to def
    t p  =  to matching paren, bracket, brace |%

    t n  = beginning of line |_
    t e  = end of file |G
    t i  = top of page |gg
    t o  = end of line |$
### Folding
    w n = left (fold all)
    w e = down (unfold one) |zo
    w i = up (fold one) |zc
    w o = right (unfold all)
### Administrative (replicated in commands)
    a a  = save |zz|:wq
    a s  = open file (this command also in daru)
    a d  = duplicate / save as (useful for duplicating files too)
    a n  = move to editor left
    a o  = move to editor right
    a k  = close editor
    a q  = quit all other editors

    // Misc
    a p  = copy absolute path of active file
    a l  = scrolloff / scroll lock
    a f  = format document / format highlighted in visual mode // DO THIS
### Unchanged keys/commands
    Esc  = escape |Esc
    ~    = switch case |~
    #    = prev occurrence of word |#
    *    = next occurrence of word |*
    ()[]{} = begin/end sentence/paragraph/section |(|)|[|]|{|}
    + -  = next/prev line |+|-
    _    = "soft" next line |_
    |    = execute more than one command in line or pipe ||
    =    = formatting / auto indenting operator |=
    R    = Replace mode |R
    r    = replace one char |r
    g    = extra commands |g
    gg   = beginning of file |gg
    G    = end of file |G
