## Unmapped Vim Commands
    jump to mark beginning of line |'
    substitute aka :s |& |:s
    append at end of line |A
    previous WORD |B
    change |c
    change from cursor to end of line |C
    delete/cut from cursor to end of line |D
    end of WORD |E
    find char |f
    find char backwards |F
    help |K
    jump to middle of screen |M
    jump to next find result |n
    jump to prev find Result |N
    new line and enter insert mode below |o
    new line and enter insert mode above |O
    paste after |P
    ex mode |Q
    substitute char, delete cur char and enter insert |s
    substitute line, delete cur line and enter insert |S
    till |t
    till backwards |T
    undo all changes on a line, one change is entering and exiting insert mode |U
    visual line mode |V
    start of next word |w
    start of next WORD |W
    delete char after cursor |x
    backspace, delete char behind cursor |X
    yank line |Y
    command digraphs that start with z |z
    quit digraph |Z
## Unmapped VsCode Commands
    move line up/ switch line with line above
    move line down /switch line with line below