const KEYBINDINGS_PATH = 'Library/Application Support/Code/User/keybindings.json';
require('./keybindings.generator')(KEYBINDINGS_PATH);
require('./settings.generator')('Library/Application Support/Code/User/settings.json');
require('./keybindings.diagnostics')(KEYBINDINGS_PATH);
