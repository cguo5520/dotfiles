const Path = require('path');
const fs = require('fs');

const HOME_DIR = process.env.HOME;

// Get the directory of the project root. This doesn't care where the script is executed.
const PROJ_DIR = Path.dirname(require.main.filename);

function getProjectRootedPath(path) {
  return `${PROJ_DIR}/${path}`;
}

function getHomePath(path) {
  return `${HOME_DIR}/${path}`;
}

// out: string
function readFile(path) {
  const projPath = getProjectRootedPath(path);
  return fs.readFileSync(projPath, { encoding: 'utf-8' });
}

// Use this to write to vscode configs
function readAbsoluteFile(path) {
  const absPath = getHomePath(path);
  return fs.readFileSync(absPath, { encoding: 'utf-8' });
}

// Used to write JSON to a file relative to the directory
function writeFile(path, objOrStr) {
  // Don't JSON.stringify strings because that escapes the \t and \n chars
  if (typeof objOrStr === 'object') {
    objOrStr = JSON.stringify(objOrStr);
  }
  const projPath = getProjectRootedPath(path);
  return fs.writeFileSync(projPath, objOrStr);
}

// Use this to write to vscode configs
function writeAbsoluteJSON(path, objOrStr) {
  const absPath = getHomePath(path);
  return fs.writeFileSync(absPath, JSON.stringify(objOrStr));
}

module.exports = {
  readFile,
  readAbsoluteFile,
  writeFile,
  writeAbsoluteJSON,
};