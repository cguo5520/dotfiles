const SPACE = ' ';
const EMPTY_CHAR = '';

function TreeNode(line) {
  const splitText = line.text.split(SPACE);
  const firstWord = splitText[0];
  this.depth = line.depth;
  switch (firstWord) {
    case 'CMD:':
      this.command = splitText[1];
      break;
    case 'WHEN:':
      this.when = splitText.slice(1).join(SPACE);
      break;
    default:
      this.key = firstWord;
      if (splitText[1]) {
        this.command = splitText[1];
      }
  }
}

/**
 * Takes a stack of parent nodes with the leaf node last
 *  and outputs the correct VsCode keybinding
 * @param {Array.<TreeNode>} stack 
 */
function Keybinding(stack) {
  this.key = stack.filter(node => node.key).map(node => node.key).join(SPACE) || undefined;
  this.command = stack.filter(node => node.command).map(node => node.command).join(EMPTY_CHAR) || undefined;
  this.when = stack.filter(node => node.when).map(node => node.when).join(' && ') || undefined;
}

function TreeParser(rawText, indentation) {
  this.rawText = rawText;
  this.indentation = indentation;
  this.rawLines = this.rawText.split('\n');
  this.commentRemovedLines = this.rawLines.map(line => line.split('//')[0]);
  this.getDepth = function (str) {
    let count = 0;
    for (let i = 0; i < str.length; i++) {
      if (str[i] === SPACE) {
        count++;
      } else {
        break;
      }
    }
    const depth = count / this.indentation;
    if (!Number.isInteger(depth)) {
      throw new Error(`invalid whitespace at START${str}END`);
    }
    return depth;
  };
  this.trimmedLines = this.commentRemovedLines.map(line => ({ depth: this.getDepth(line), text: line.trim() }));

  this.getKeybindings = function () {
    const keybindings = [];
    let stack = [];
    this.trimmedLines.forEach(line => {
      const curDepth = stack.length - 1;
      const treeNode = new TreeNode(line);
      const prevWasLeafNode = curDepth >= treeNode.depth;
      if (prevWasLeafNode) {
        keybindings.push(new Keybinding(stack));
        stack = stack.slice(0, treeNode.depth);
      }
      stack.push(treeNode);
    });
    return keybindings;
  };
};

module.exports = TreeParser;