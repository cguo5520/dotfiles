let unmappings = require('./unmap.all.keybindings.json');

unmappings = unmappings.map(unmapping => { delete (unmapping.when); return unmapping })

temp = {}

unmappings.forEach(unmapping => {
  temp[unmapping.key + unmapping.command] = unmapping
})

deduped = []

for (let key in temp) {
  deduped = [...deduped, temp[key]]
}

writeFile('outputs/keybindings.output.json', deduped);