module.exports = function (outputPath) {
  const TreeParser = require('./parser.js');
  const { readFile, writeFile, writeAbsoluteJSON } = require('../fsUtil');

  const rawText = readFile('keybindings.generator/tree.txt');
  const unmappings = require('./unmap.all.keybindings.json');

  const INDENTATION = 2;
  const treeParser = new TreeParser(rawText, INDENTATION);

  const keybindings = treeParser.getKeybindings();

  keybindings.push(...unmappings);

  writeFile('outputs/keybindings.output.json', keybindings);
  writeAbsoluteJSON(outputPath, keybindings);

  console.log('successfully overwrote VsCode keybindings');
}
