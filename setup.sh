#!/usr/bin/env bash
set -e

tags="$1"

if [ -z $tags ]; then
  tags="all"
fi

if ! type ansible > /dev/null; then
  echo "Installing Ansible..."
  sudo apt install ansible -y
fi

ansible-playbook ~/dotfiles/ansible/main.yml --ask-become-pass --tags $tags
