" Begin .vimrc

set nocompatible " This option stops vim from behaving in a strongly vi -compatible way. It should be at the start of any vimrc file as it can affect lots of other options which you may want to override.
set backspace=2 " Backspace with this value allows to use the backspace character (aka CTRL-H or "<-") to use for moving the cursor over automatically inserted indentation and over the start/end of line. (see also the whichwrap option)
" set columns=80 " Unset column width means that it fits to terminal width
" set wrapmargin=0 " Do not add margin
syntax on " Syntax highlighting
set ruler " Line and column at the bottom right of the terminal 
set textwidth=80 " Automatically breakline when text goes over 80 chars
set whichwrap=<,>,h,l
set autoindent " autoindent lines

mapclear

noremap n h
noremap e j
noremap i k
noremap o l

noremap N b
noremap E 10j
noremap I 10k
noremap O e

noremap tn ^
noremap te G
noremap ti gg
noremap to $

noremap u i
noremap y a

noremap z u
noremap Z <C-r>

noremap r r

noremap v "+p

nnoremap m v " visual mode
nnoremap , V " line mode
nnoremap b <C-v> " block mode
nnoremap . R " replace mode

nnoremap cc "+yy
nnoremap cw "+yaw
nnoremap cn "+y^
nnoremap co "+y$

nnoremap xx "+dd
nnoremap xw "+daw
nnoremap xn "+d^
nnoremap xo "+d$

nnoremap dd "_dd
nnoremap dw "_daw
nnoremap dn "_d^
nnoremap do "_d$

vnoremap x "+d
vnoremap X "*d
vnoremap c "+y
vnoremap C "*y
vnoremap d "_d

noremap tb %

noremap ll J
noremap ln i<CR><Esc>
noremap le $a<CR><Esc>
noremap li 0i<CR><Esc>
noremap lo a<CR><Esc>

nnoremap <BS> i<BS><Esc>`^ " Make backspace work in normal mode

" remaps ctrl-space to escape
noremap <C-@> <Esc>
inoremap <C-@> <Esc>
cnoremap <C-@> <Esc>

noremap aq :qa!<CR> " quit without warning and without saving
noremap aa :w<CR> " quit without warning and without saving
noremap ak :q!<CR> " quit without warning and without saving

" End .vimrc