#!/usr/bin/env bash
set -e

tags="$1"

if [ -z $tags ]; then
  tags="all"
fi

ansible-playbook ~/dotfiles/ansible/post_setup.yml --ask-become-pass --tags $tags

