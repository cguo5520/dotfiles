#!/bin/bash

# MAIN
echo "Uninstalling Guomak"

rm -rf /usr/share/X11/xkb/*
cp -R xkb_clean/* /usr/share/X11/xkb

rm /etc/default/keyboard
cp etc_default_keyboard_colemak /etc/default/keyboard

# Keyboard
gsettings set org.gnome.desktop.input-sources mru-sources "[('xkb', 'us+colemak')]"
gsettings set org.gnome.desktop.input-sources sources "[('xkb', 'us+colemak')]"

echo "Success"
