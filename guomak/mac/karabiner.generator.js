const fs = require('fs');

function parseKeybindings(file) {
  return file.toString().split('\n').map(line => line.split(' '))
}

const level1 = fs.readFileSync('./level1');
const level1KeyTuples = parseKeybindings(level1);
const level3 = fs.readFileSync('./level3');
const level3KeyTuples = parseKeybindings(level3);
const level4 = fs.readFileSync('./level4');
const level4KeyTuples = parseKeybindings(level4);

const simple_modifications = level1KeyTuples.map(([from, to]) => {
  return {
    "from": {
      "key_code": from,
    },
    "to": {
      "key_code": to,
    }
  };
});

const LEVEL_3_MODIFIER_NAME = 'virtual_mod_0';
const LEVEL_4_MODIFIER_NAME = 'virtual_mod_1';
const LEVEL_3_MODIFIER_KEYS = ['close_bracket'];
const LEVEL_4_MODIFIER_KEYS = ['tab'];

const virtualModManipulators = [
  [LEVEL_3_MODIFIER_NAME, LEVEL_3_MODIFIER_KEYS],
  [LEVEL_4_MODIFIER_NAME, LEVEL_4_MODIFIER_KEYS]
].map(([modifierName, keys]) => (keys.map(key => {
  return {
    "type": "basic",
    "from": {
      "key_code": key,
      // "modifiers": // omission means that this only works when modifiers aren't pressed
    },
    "to": [
      {
        "set_variable": {
          "name": modifierName,
          "value": 1
        }
      }
    ],
    "to_after_key_up": [
      {
        "set_variable": {
          "name": modifierName,
          "value": 0
        }
      }
    ],
    "conditions": [
      {
        "type": "variable_if",
        "name": LEVEL_3_MODIFIER_NAME,
        "value": 0
      },
      {
        "type": "variable_if",
        "name": LEVEL_4_MODIFIER_NAME,
        "value": 0
      },
    ]
  };
}))).flat();

const virtualModRule = {
  description: "Virtual Modifiers",
  manipulators: virtualModManipulators,
};

virtualModRule.manipulators.push({
  "type": "basic",
  "from": {
    "key_code": 'quote',
    "modifiers": {
      "mandatory": ['control'],
      "optional": ["any"]
    }
  },
  "to": [{
    "key_code": 'tab',
    modifiers: ['left_command'],
  }]
});

virtualModRule.manipulators.push({
  "type": "basic",
  "from": {
    "key_code": 'open_bracket',
    "modifiers": {
      "mandatory": ['control'],
      "optional": ["any"]
    }
  },
  "to": [{
    "key_code": 'tab',
    modifiers: ['left_command', 'left_shift'],
  }]
});

virtualModRule.manipulators.push({
  "type": "basic",
  "from": {
    "key_code": 'open_bracket',
    // "modifiers": // omission means no mods. This needs to be a complex mod instead of simple because of this line. It conflicts with the command, leftshift overwrite above
  },
  "to": [{
    "key_code": 'delete_or_backspace',
  }]
});

virtualModRule.manipulators.push({
  type: 'basic',
  from: {
    key_code: 'spacebar',
    modifiers: { mandatory: ['shift'] },
  },
  to: [{ key_code: 'equal_sign' }],
})

function keyTuplesToMani(keyTuples, modifierName) {
  return keyTuples.map(([from, to, shift]) => {
    const mani = {
      "type": "basic",
      "from": {
        "key_code": from,
      },
      "to": [
        {
          "key_code": to
        }
      ],
      "conditions": [
        {
          "type": "variable_if",
          "name": modifierName,
          "value": 1
        }
      ]
    };

    if (shift === 'shift') {
      mani.to[0].modifiers = ['left_shift'];
    }
    return mani;
  });
}

const level3Rules = {
  description: "Level 3 keybindings",
  manipulators: keyTuplesToMani(level3KeyTuples, LEVEL_3_MODIFIER_NAME)
};

const level4Rules = {
  description: "Level 4 keybindings",
  manipulators: keyTuplesToMani(level4KeyTuples, LEVEL_4_MODIFIER_NAME)
};

const complex_modifications = {
  "parameters": {
    "basic.simultaneous_threshold_milliseconds": 50,
    "basic.to_delayed_action_delay_milliseconds": 500,
    "basic.to_if_alone_timeout_milliseconds": 1000,
    "basic.to_if_held_down_threshold_milliseconds": 500,
    "mouse_motion_to_scroll.speed": 100
  },
  "rules": [
    virtualModRule, level3Rules, level4Rules
  ]
};

const guomakProfile = {
  name: "Guomak",
  simple_modifications,
  complex_modifications,
  devices: [],
  "fn_function_keys": [
    {
      "from": {
        "key_code": "f1"
      },
      "to": {
        "consumer_key_code": "display_brightness_decrement"
      }
    },
    {
      "from": {
        "key_code": "f2"
      },
      "to": {
        "consumer_key_code": "display_brightness_increment"
      }
    },
    {
      "from": {
        "key_code": "f3"
      },
      "to": {
        "key_code": "mission_control"
      }
    },
    {
      "from": {
        "key_code": "f4"
      },
      "to": {
        "key_code": "launchpad"
      }
    },
    {
      "from": {
        "key_code": "f5"
      },
      "to": {
        "key_code": "illumination_decrement"
      }
    },
    {
      "from": {
        "key_code": "f6"
      },
      "to": {
        "key_code": "illumination_increment"
      }
    },
    {
      "from": {
        "key_code": "f7"
      },
      "to": {
        "consumer_key_code": "rewind"
      }
    },
    {
      "from": {
        "key_code": "f8"
      },
      "to": {
        "consumer_key_code": "play_or_pause"
      }
    },
    {
      "from": {
        "key_code": "f9"
      },
      "to": {
        "consumer_key_code": "fastforward"
      }
    },
    {
      "from": {
        "key_code": "f10"
      },
      "to": {
        "consumer_key_code": "mute"
      }
    },
    {
      "from": {
        "key_code": "f11"
      },
      "to": {
        "consumer_key_code": "volume_decrement"
      }
    },
    {
      "from": {
        "key_code": "f12"
      },
      "to": {
        "consumer_key_code": "volume_increment"
      }
    }
  ],
  parameters: {
    "delay_milliseconds_before_open_device": 1000
  },
  selected: true,
  virtual_hid_keyboard: {
    "country_code": 0,
    "mouse_key_xy_scale": 100
  }
};

// add misc constants
const cfg = {
  "global": {
    "check_for_updates_on_startup": true,
    "show_in_menu_bar": true,
    "show_profile_name_in_menu_bar": false
  },
  "profiles": [guomakProfile]
};

// export JSON
fs.writeFileSync('./karabiner.json', JSON.stringify(cfg));
