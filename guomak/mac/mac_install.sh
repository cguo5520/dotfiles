#!/bin/bash

node karabiner.generator
cp karabiner.json ~/.config/karabiner/karabiner.json

defaults write -g ApplePressAndHoldEnabled -bool false
